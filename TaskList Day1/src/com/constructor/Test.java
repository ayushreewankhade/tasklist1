package com.constructor;

public class Test{
 int rollNum;
 String studentName;

 Test(int num,String name){
     rollNum=num;
     studentName=name;
}

public static void main(String[] args){
 Test test=new Test(10,"Velocity");

 System.out.println("Student RollNum is "+test.rollNum);
 System.out.println("Student Name is "+test.studentName);
}
}
