package com.staticVariableAndMethod;

public class TestStaticVariable {
	int a=5;
	static int b=5;
	public static void main(String args[]){
	TestStaticVariable sd2=new TestStaticVariable();
	System.out.println("nonstatic>>"+sd2.a++);
	System.out.println("static>>"+sd2.b++);
	
	TestStaticVariable sd3=new TestStaticVariable();
	System.out.println("nonstatic>>"+sd3.a++);
	System.out.println("static>>"+sd3.b++);
	
	TestStaticVariable sd4=new TestStaticVariable();
	System.out.println("nonstatic>>"+sd4.a++);
	System.out.println("static>>"+sd4.b++);
	
	TestStaticVariable sd5=new TestStaticVariable();
	System.out.println("nonstatic>>"+sd5.a++);
	System.out.println("static>>"+sd5.b++);
	}
}