package com.tryCatchFinally;

public class ExceptionDemo {
	public static void main(String[] args){
		
		System.out.println("Firstline");
		System.out.println("Secondline");
		System.out.println("Thirdline");
		
		try{
			System.out.println(":: Try block ::");
		int a=10/0;
		}
		catch(ArithmeticException e){
			System.out.println(":: Catch block Arithmetic Exception ::");
		    System.out.println(e);
		}
		
		finally {
            System.out.println (":: Finally Block::");
		}
}
}
