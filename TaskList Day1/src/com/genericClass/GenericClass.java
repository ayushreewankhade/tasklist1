package com.genericClass;

public class GenericClass<T1, T2>{
	
	
	public void m1(T1 obj1, T2 obj2 ) {
		System.out.println("Id: " +obj1+ " || Name: " +obj2 );
	}
	
 public static void main(String args[]) {
	 GenericClass<Integer, String> obj= new GenericClass<Integer, String>();
	 obj.m1(101, "Ram");
 }
}
