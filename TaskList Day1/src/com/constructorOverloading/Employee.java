package com.constructorOverloading;

public class Employee {  
		int id;  
		String name;  
		  
		Employee(){  
		System.out.println("this a default constructor");  
		}  
		  
		Employee(int i, String n){  
		id = i;  
		name = n;  
		}  
		  
		public static void main(String[] args) {  
			
			Employee e = new Employee();  
		System.out.println("Default Constructor values: ");  
		System.out.println("Employee Id : "+e.id + " Employee Name : "+e.name);  
		  
		System.out.println("Parameterized Constructor values: ");  
		Employee emp = new Employee(101, "Ram");  
		System.out.println("Employee Id : "+emp.id + " Employee Name : "+emp.name);  
		}  
	}  

