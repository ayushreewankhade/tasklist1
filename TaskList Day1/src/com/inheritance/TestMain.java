package com.inheritance;

public class TestMain {

	public static void main(String[] args) {
		Child child=new Child();
		Parent parent=new Parent();
		
		System.out.println(parent.a);
		System.out.println(parent.b);
		System.out.println(child.a);
		System.out.println(child.b);
		System.out.println(child.c);
		
		parent.m1();
		parent.m2();
		child.m1();
		child.m2();
		child.m3();
		

	}

}
