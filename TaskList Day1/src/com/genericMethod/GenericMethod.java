package com.genericMethod;

public class GenericMethod {
 public static <T extends Comparable<T>> T max(T num1, T num2) {
	  return(num1.compareTo(num2) < 0) ? num2 : num1;
 }
}
